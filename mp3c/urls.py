from django.urls import include, path
from django.views.generic import RedirectView

from mp3c import views

app_name = 'mp3c'

app_name = 'mp3c'
urlpatterns = [
    path('', RedirectView.as_view(pattern_name='mp3c:asteroid-list')),
    path('asteroids/', views.asteroid_list, name='asteroid-list'),
    path('asteroids/<int:pk>/', views.asteroid_detail, name='asteroid-detail'),
]
