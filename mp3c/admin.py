from django.contrib import admin

from mp3c.models import Asteroid, Mass

class MassInline(admin.TabularInline):
    model = Mass


@admin.register(Asteroid)
class AsteroidAdmin(admin.ModelAdmin):
    inlines = [MassInline]
