from django.db import models
from django.urls import reverse


class Asteroid(models.Model):
    name = models.CharField(
        unique=True,
        max_length=64,
    )

    def get_absolute_url(self):
        return reverse('mp3c:asteroid-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name


class Mass(models.Model):
    asteroid = models.ForeignKey(Asteroid, models.CASCADE)
    value = models.FloatField()
    uncertainty = models.FloatField(null=True, blank=True)
    reference = models.CharField(max_length=64)

    def __str__(self):
        return '%0.3g' % self.value
