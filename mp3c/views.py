from django.shortcuts import get_object_or_404, render

from mp3c.models import Asteroid


def asteroid_list(request):
    context = {'asteroids': Asteroid.objects.all()}
    response = render(request, 'asteroid_list.html', context)
    return response


def asteroid_detail(request, pk):
    asteroid = get_object_or_404(Asteroid, pk=pk)
    context = {'asteroid': asteroid}
    response = render(request, 'asteroid_detail.html', context)
    return response
