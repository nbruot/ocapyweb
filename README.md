# OCAPY web project demo with mini Minor Planet Physical Properties Catalogue (MP3C) app

## Installation

### Python environment

You must use a Python version at least equal to 3.6.


#### Linux

`cd` into the cloned repository and execute:

```
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install django
```


#### Windows with Anaconda3

Execute in an Anaconda prompt:

```
> conda create -n ocapyweb
> conda activate ocapyweb
> conda install django
```


### Database

This project is configured to use a SQLite3 file as the database so that you do not need to install a database engine. Initialize the database with:

```
$ python manage.py migrate
```

To be able to connect to the admin site, create a superuser account:

```
$ python manage.py createsuperuser
```


### Run the development server

To start using the website with the development server, launch:

```
$ python manage.py runserver
```

You can create a few asteroids in the admin site. Alternatively, you can use the Python shell with `python manage.py shell` and execute lines such as:

```
>>> from mp3c.models import *
>>> ceres = Asteroid.objects.create(name='Ceres')
>>> Mass.objects.create(asteroid=ceres, value=9.35e20, reference='2001A&A...365..627G')
<Mass: Mass object (1)>
>>> Mass.objects.create(asteroid=ceres, value=9.384e20, uncertainty=1e+17, reference='2016Sci...353.1008R')
<Mass: Mass object (2)>
>>>
```
